#!/bin/sh

set -eu

# setup password encrypting and storing git credential helper

# bad check for if secret.service is provided or not
if [ "${DISPLAY-} ${WAYLAND_DISPLAY-}" != " " ] \
&& [ -e /usr/share/dbus-1/services/org.freedesktop.impl.portal.Secret.service ]
then
	case "${distro-}" in
		debian)
			sudo apt-get install -y libsecret-1-0 libsecret-1-dev gnome-keyring
			sudo make --directory=/usr/share/doc/git/contrib/credential/libsecret
			;;
		garuda|arch)
			sudo make --directory=/usr/share/git/credential/libsecret
			;;
		*)
			echo "$0: unknown distro"
			exit 1
			;;
	esac
fi

# set Git configs to use above

cd "$(dirname "$(realpath "$0")")"

case "${distro-}" in
	debian)
		cat <<-EOF | tee credential.inc
		[credential]
		        # encrypted password store
		        helper = /usr/share/doc/git/contrib/credential/libsecret/git-credential-libsecret
		EOF
		;;
	garuda|arch)
		cat <<-EOF | tee credential.inc
		[credential]
		        # encrypted password store
		        helper = /usr/share/git/credential/libsecret/git-credential-libsecret
		EOF
		;;
	*)
		"$0: unknown distro"
		exit 1
		;;
esac








# Notes for running secret.service in a headless tty.
# I doubt I'll do this but maybe.
# Using keepassxc is likely easier than this.



#yum -y install gnome-keyring libsecret dbus-x11

#eval "$(dbus-launch --sh-syntax)"

#mkdir -p ~/.cache
#mkdir -p ~/.local/share/keyrings # where the automatic keyring is created

# 1. Create the keyring manually with a dummy password in stdin
#eval "$(printf '\n' | gnome-keyring-daemon --unlock)"

# 2. Start the daemon, using the password to unlock the just-created keyring:
#eval "$(printf '\n' | /usr/bin/gnome-keyring-daemon --start)"
